import Server.OpenCV.Image_processing as ImageP
import Server.Equation_Solve.eq_solve as Solver
from Server.Model import Model as Model
import os
import json
import cv2
import numpy as np


class ModelLogic:
    TYPE_config = {
        "type": "tensorflow",
        "DBTrain": "../../DataBase/dataset/train/"
    }

    model_config = {
        "load": True,
        "path": "../Model/Photomath_model_VGG16.h5",
        "epochs": 2,
        "DataBaseConfig": "../../DataBase/DBConfig.json"
    }

    def __init__(self):
        self.model = self.__SelectModel()
        self.config = open(self.model_config["DataBaseConfig"], "r")
        self.config = json.load(self.config)
        self.labels = self.config["class"]["labels"]
        del self.config

    def SolveImage(self, pathImage):
        Im_P = ImageP.Image_processing(pathImage)
        Im_P.create_dir()
        Im_P.split_image()
        path_images = Im_P.getPathImages()
        img_name = Im_P.get_image_name()
        equ = self.__use_model(self.model, path_images, img_name)
        equ = self.__correct_equation(equ)
        print(equ)
        try:
            answer = self.__solver(equ)
        except:
            answer = 'None'

        Im_P.delete_pictures()
        return answer

    def __solver(self, equ):
        equation = "".join(equ)
        eq_solver = Solver.Equation_Solver(equation)
        return eq_solver.solve_equation()

    def __use_model(self, model: Model.Model, path_images, img_name):
        files = os.listdir(path_images)

        images = []

        for file in files:
            file_name, file_ext = os.path.splitext(file)
            if file_ext == ".png":
                full_path = "Split_Data/" + img_name + "/" + file_name + file_ext
                images.append(full_path)

        equation = ""
        for image in images:
            prediction = model.predict(image)
            prediction = self.labels[prediction]
            print(prediction)
            if prediction == 'forward_slash':
                prediction = '/'
            elif prediction == 'times':
                prediction = 'x'
            equation += prediction

        print(equation)
        return equation

    def __correct_equation(self, equ):
        """Function will check if the equation is ok and will return True/False"""
        if equ.count('=') > 1: equ = equ.replace('=', '', equ.count('=') - 1)
        if '=' in equ and 'y' in equ: return equ

        if 'y' not in equ:
            equ = 'y + ' + equ

        if '=' not in equ and '-' in equ:
            equ = equ.replace('-', '=')
        return equ

    def __SelectModel(self):
        model = Model.Tensorflow_model()
        if self.model_config["load"]:
            model.load_model(self.model_config["path"])
        else:
            model.create_model()
        return model
