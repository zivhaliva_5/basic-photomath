import matplotlib.pyplot as plt
import numpy as np
import cv2
import os

import tensorflow as tf
from tensorflow import keras
from keras import layers
from keras.layers import Dense, Flatten, Conv2D, Activation, Dropout
from keras import backend as K
from keras.models import Sequential, Model
from keras.models import load_model
from keras.optimizers import SGD
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import MaxPool2D
from keras.preprocessing.image import ImageDataGenerator

from abc import ABC, abstractmethod

DATASET_PATH = "../../DataBase/dataset/"
TRAIN_PATH = DATASET_PATH + "train/"
TEST_PATH = DATASET_PATH + "test/"
BATCH_SIZE = 32
IMG_HEIGHT = 224
IMG_WIDTH = 224
IMG_SIZE = (IMG_WIDTH, IMG_HEIGHT)


class Model(ABC):
    @abstractmethod
    def create_model(self):
        pass

    @abstractmethod
    def load_model(self, model_path: str):
        pass

    @abstractmethod
    def train_model(self, epochs: int):
        pass


class Tensorflow_model(Model):

    def __init__(self, train_ds_path=TRAIN_PATH, val_ds_path=TEST_PATH, batch_size=BATCH_SIZE, img_size=IMG_SIZE):
        self.train_ds_path = train_ds_path
        self.val_ds_path = val_ds_path
        self.batch_size = batch_size
        self.img_size = img_size
        self.model = None
        self.labels = None
        self.model_path = "Model/Photomath_model_VGG16.h5"

    def create_model(self):
        if os.path.exists(self.model_path):
            self.model = self.load_model(self.model_path)
            return

        # create VGG16 model:
        self.model = Sequential([
            Conv2D(input_shape=(IMG_HEIGHT, IMG_WIDTH, 3), filters=64, kernel_size=(3, 3), padding='same',
                   activation='relu'),
            Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu'),
            MaxPool2D(pool_size=(2, 2), strides=(2, 2)),
            Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu'),
            Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu'),
            MaxPool2D(pool_size=(2, 2), strides=(2, 2)),
            Conv2D(filters=256, kernel_size=(3, 3), padding='same', activation='relu'),
            Conv2D(filters=256, kernel_size=(3, 3), padding='same', activation='relu'),
            Conv2D(filters=256, kernel_size=(3, 3), padding='same', activation='relu'),
            MaxPool2D(pool_size=(2, 2), strides=(2, 2)),
            Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu'),
            Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu'),
            Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu'),
            MaxPool2D(pool_size=(2, 2), strides=(2, 2)),
            Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu'),
            Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu'),
            Conv2D(filters=512, kernel_size=(3, 3), padding='same', activation='relu'),
            MaxPool2D(pool_size=(2, 2), strides=(2, 2), name='vgg16'),
            Flatten(name='flatten'),
            Dense(256, activation='relu', name='fc1'),
            Dense(128, activation='relu', name='fc2'),
            Dense(16, activation='softmax', name='output'),
        ])

        epochs = 12
        history = self.train_model(epochs)
        self.visualizeResults(history, epochs)

    def visualizeResults(self, history, epochs):
        """Function will visualize the results of the model using history
        :param history: the results
        :param epochs: number of epochs
        :type epochs: int
        :return: none"""
        acc = history.history['accuracy']
        val_acc = history.history['val_accuracy']

        loss = history.history['loss']
        val_loss = history.history['val_loss']

        epochs_range = range(epochs)

        plt.figure(figsize=(8, 8))
        plt.subplot(1, 2, 1)
        plt.plot(epochs_range, acc, label='Training Accuracy')
        plt.plot(epochs_range, val_acc, label='Validation Accuracy')
        plt.legend(loc='lower right')
        plt.title('Training and Validation Accuracy')

        plt.subplot(1, 2, 2)
        plt.plot(epochs_range, loss, label='Training Loss')
        plt.plot(epochs_range, val_loss, label='Validation Loss')
        plt.legend(loc='upper right')
        plt.title('Training and Validation Loss')
        plt.show()

    def load_model(self, model_path):
        """Function will load the model from dictionary and will return it.
        :param model_path: the path to the existing model"""
        self.model = tf.keras.models.load_model(model_path)
        self.model_path = model_path

    def train_model(self, epochs):
        """Function will train the model
        :param epochs: number of epochs to train"""
        train_datagen = ImageDataGenerator(zoom_range=0.15, width_shift_range=0.2, height_shift_range=0.2,
                                           shear_range=0.15)

        test_datagen = ImageDataGenerator()

        train_generator = train_datagen.flow_from_directory(TRAIN_PATH, target_size=self.img_size,
                                                            batch_size=self.batch_size,
                                                            shuffle=True, class_mode='categorical')

        test_generator = test_datagen.flow_from_directory(TEST_PATH, target_size=self.img_size,
                                                          batch_size=self.batch_size,
                                                          shuffle=False, class_mode='categorical')

        Vgg16 = keras.models.Model(inputs=self.model.input, outputs=self.model.get_layer('vgg16').output)
        Vgg16.load_weights("vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5")
        es = EarlyStopping(monitor='val_accuracy', mode='max', verbose=1, patience=20)
        opt = SGD(learning_rate=1e-6, momentum=0.9)

        self.model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
        for layer in Vgg16.layers:
            layer.trainable = False

        history = self.model.fit_generator(train_generator, validation_data=test_generator, epochs=epochs, verbose=1,
                                           callbacks=es)
        self.model.save(self.model_path)
        return history

    def predict(self, img_path: str):
        image = cv2.imread(img_path)
        image = cv2.resize(image, self.img_size)
        image = np.reshape(image, (1, IMG_HEIGHT, IMG_WIDTH, 3))
        pred = self.model.predict(image)
        pred = np.argmax(pred, axis=1)[0]
        return pred
