import os
import shutil

import cv2
from imutils import contours

class Image_processing:
    '''
    class for taking an image and spiriting letters and numbers
    the class first takes an image path that will be provided by the user
    in the constructor path,name of the image and path of where the split images will be outputted
    '''

    def __init__(self, image_path:str):
        self.color = (0,255,0)
        self.thickness = 0
        self.size = (224,224)

        #variables used for reading and saving image
        self.image_path = image_path
        img_list = self.image_path.split("\\")
        self.image_name = img_list[ len(img_list) - 1 ].split('.')[0]
        self.path = "Split_Data/"+self.image_name

        #used to find countors in images
        self.image = cv2.imread(self.image_path)
        self.image = cv2.resize(self.image,(500, 300), cv2.INTER_CUBIC)
        self.gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        self.thresh = cv2.threshold(self.gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_TRIANGLE)[1]
        
    def save_img(self, name, img):
        cv2.imwrite(os.path.join(self.path ,name), img)
    '''
    create the directory of the split images by the image name
    '''
    def create_dir(self):
        if os.path.exists(self.path) == False:
            os.makedirs(self.path)

    def __create_image(self, name, x,y,w,h):

        cv2.rectangle(self.image, (x, y), (x + w, y + h), color=self.color, thickness=self.thickness)
        ROI = 255 - self.thresh[y:y + h, x:x + w]
        ROI = cv2.resize(ROI, self.size, interpolation=cv2.INTER_LINEAR)

        self.save_img(name, ROI)

    def __split_images_and_resize(self, contours):
        
        '''
        function cuts the contours from image
        '''
        ROI_number = 0
        for contour in contours:
            x = contour[0]
            y = contour[1]
            w = contour[2]
            h = contour[3]
            self.__create_image("part_" + str(ROI_number) + ".png", x,y,w,h)
            ROI_number += 1

    def __exeption_equals(self, before, current):

        '''
        @point (x,y) is the top left corner of the image.
        @width is on the x axis to the right as normal.
        @height is on th y axis down ways.
        As we want to contour the entire symbol we need higher y axis,
        so we need to subtract in this 10 is enough.
        As there is white space between the lines of the symbol,
        multiplying is required as the space generally is small.
        '''

        x = min(before[0],current[0])
        y = min(before[1],current[1])
        w = max(before[2],current[2]) + (max(before[0],current[0]) - min(before[0],current[0]))
        h = before[3] + current[3] + (max(before[1],current[1]) - min(before[1],current[1]))

        return [x,y,w,h]

    def split_image(self):

        cnts = cv2.findContours(self.thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
        (cnts, _) = contours.sort_contours(cnts, method="left-to-right")

        cons = []
        index = 0
        for con in cnts:
            area = cv2.contourArea(con)
            if area < 45:
                continue

            x, y, w, h = cv2.boundingRect(con)
            """# Ignore contours that are too small or too large
            if w > self.image.shape[1] * 0.8 or h > self.image.shape[0] * 0.8:
                continue"""

            index = len(cons) - 1
            #exeption for = because it's two - very close to each other
            if index > 0:
                diff_height = max(h, cons[index][3]) - min(h, cons[index][3])
                diff_width = max(w, cons[index][2]) - min(w, cons[index][2])
                if diff_height < 10 and diff_width < 10:
                    cons[index] = self.__exeption_equals(cons[index],   [x,y,w,h])


            #if the contour isn't in other contour
            inside = -1
            for cnt2 in cons:
                [x2, y2, w2, h2] = cnt2
                p1 = (x2,y2)
                p2 = (x2+w2,y2+h2)
                p3 = (x,y)
                in_between = (p1[0] <= p3[0] <= p2[0] or p2[0] <= p3[0] <= p1[0]) and \
                             (p1[1] <= p3[1] <= p2[1] or p2[1] <= p3[1] <= p1[1])
                if in_between:
                    inside = 0

            if inside == -1:
                cons.append([x, y, w, h])

        self.__split_images_and_resize(cons)
        print()

    def getPathImages(self):
        return self.path

    def delete_pictures(self):
        """Function will delete the folder of the original picture.
        the function should be used after the prediction is all done. """
        shutil.rmtree(self.path)

    def get_image_name(self):
        return self.image_name