import re
from sympy import sympify, solve


class Equation_Solver:

    def __init__(self, eq):
        self.equ = eq

    def __adjust_equation(self):
        """Function will fix the equation so it could be handled by sympy.sympify function
        for example, for input of 2y + 2x2 = 12, the function will return 2*y + 2*2 , 12
        ****y is the variable sign, and not x, x is the multiplication signature"""
        self.equ = self.equ.replace("x", "*")
        self.equ = self.equ.replace("=", ",")
        index_of_y = [m.start() for m in re.finditer('y', self.equ)]  # find all occurrences of variable y
        if len(index_of_y) != 0:
            for index in index_of_y:
                if index > 0 and self.equ[index - 1] in '0123456789':
                    self.equ = self.equ[:index] + "*" + self.equ[index:]
                    for i in range(0, len(index_of_y)):
                        index_of_y[i] = index_of_y[i] + 1

    def solve_equation(self):
        """Function will solve the equation and will return a list with the answers."""
        self.__adjust_equation()
        sympy_eq = sympify("Eq(" + self.equ + ")")
        results = str(solve(sympy_eq))
        return results
