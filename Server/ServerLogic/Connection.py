import Server.Model.ModelLogic as ML
import socket
import json


class custom_server:
    PORT = None
    CODES = {
        "RequestKey": 99,
        "SolveSimpleEquation": 100,
        "Error": 254,
        "Exit": 255
    }

    def __init__(self):
        self.Model = ML.ModelLogic()

        with open('../../config.json') as f:
            config = json.load(f)

        self.PORT = config["port"]

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ("127.0.0.1", self.PORT)
        sock.bind(server_address)
        sock.listen(1)
        print('waiting for a connection...')
        self.connection, self.client_address = sock.accept()
        print(f'connection from {self.client_address}')

    def __convertSTRNumberTo4Bytes(self, length):
        """
        :param - length
        takes a string of numbers and adds 0 up to length of 4
        """
        if len(length) > 4:
            return -1

        while len(length) < 4:
            length = " " + length
        return length

    def handelRequest(self, code, data):
        if code == self.CODES["RequestKey"]:
            return 0, 0

        if code == self.CODES["SolveSimpleEquation"]:
            answer = self.Model.SolveImage(data)
            r_code = chr(self.CODES["SolveSimpleEquation"])
            r_length = self.__convertSTRNumberTo4Bytes(str(len(answer)))
            msg = r_code + r_length + answer
            return 0, msg

        if code == self.CODES["Exit"]:
            return -1, self.CODES["Exit"] + "0000"

    def SendMessage(self, msg):
        self.connection.send(msg.encode())

    def RecvMessage(self):
        try:
            data = self.connection.recv(5).decode()
            code = data[0]
            length_data = int("".join([data[1], data[2], data[3], data[4]]))
            del data
            data = self.connection.recv(length_data).decode()
            """
            returns - request code, length of data, data
            """
            return [ord(code), int(length_data), data]
        except:

            return []

    def closeServer(self):
        self.connection.close()
