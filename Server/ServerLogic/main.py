import Connection


def main():
    server = Connection.custom_server()
    code = 0

    while code != 255:
        code, _, data = server.RecvMessage()
        exitcode, msg = server.handelRequest(code, data)
        server.SendMessage(msg)

    server.closeServer()

if __name__ == '__main__':
    main()
