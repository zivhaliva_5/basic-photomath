﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Client
{
    public partial class Main : Form
    {

        string imagePath;
        ClientHandler handler;
        Font pathFont = new Font("Ariel", 5);
        Font messFont = new Font("Ariel", 12);

        enum Codes
        {
            RequestKey = 99,
            SolveSimpleEquation = 100,
            Error = 254,
            Exit = 255
        }

        public Main()
        {
            InitializeComponent();
            this.label1.Hide();
            handler = new ClientHandler();
            handler.Connect();
        }

        private string STRIntTo4Bytes(int length)
        {
            string strL = length.ToString();

            while(strL.Length < 4)
            {
                strL = " " + strL;
            }
            return strL;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileOpen = new OpenFileDialog();

            fileOpen.Title = "Open Image file";

            fileOpen.Filter = "JPG Files (*.jpg)| *.jpg";
            fileOpen.Filter += "| PNG Files (*.png)| *.png";

            
            if (fileOpen.ShowDialog() == DialogResult.OK)
            {
                imagePath = fileOpen.FileName;
                this.label1.Hide();
                this.label1.Text = imagePath;
                this.label1.Font = pathFont;
                this.label1.Show();
            }
            fileOpen.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string msg = "";
            if (this.imagePath == null)
            {
                MessageBox.Show("Please upload Image first.");
                return;
            }
            string length = this.STRIntTo4Bytes(this.imagePath.Length);
            msg += (char)(Codes.SolveSimpleEquation) + length;
            msg += this.imagePath;

            this.label1.Hide();
            this.label1.Text = "Sending...";
            this.label1.Font = messFont;
            this.label1.Show();
            this.handler.Send(Encoding.UTF8.GetBytes(msg));
            Data data = this.handler.Receive();

            if (data.code == (int)Codes.Error)
            {
                MessageBox.Show(data.data);
            }
            else
                AnswerBox.Items.Add(data.data);
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            handler.Disconnect();
        }
    }
}
