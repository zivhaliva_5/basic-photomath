﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace Client
{
    public class config
    {
        public int port { get; set; }
    }

    class Data
    {
        public int code;
        public int length;
        public string data;

        public Data(int code, int length, byte[] data)
        {
            this.code = code;
            this.length = length;
            this.DataToString(data);
        }

        private void DataToString(byte[] data)
        {
            string msg = "";

            foreach (byte b in data)
            {
                msg += ((char)b).ToString();
            }
            this.data = msg;
        }

    }



    class ClientHandler
    {
        private Socket socket;

        public void Connect()
        {
            string jsonString = File.ReadAllText(@"../../../../Config.json");
            config con = JsonConvert.DeserializeObject<config>(jsonString);
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), con.port));
        }


        public void Send(byte[] message)
        {
            this.socket.Send(message);
        }

        public Data Receive()
        {
            byte[] buffer = new byte[5];
            this.socket.Receive(buffer);
            if (buffer[0] != 0)
            {
                int code = (int)((char)buffer[0]);
                string str_length = ((char)buffer[1]).ToString() +  ((char)buffer[2]).ToString() + ((char)buffer[3]).ToString() + ((char)buffer[4]).ToString();
                int length = int.Parse(str_length);
                byte[] newBuffer = new byte[length];
                this.socket.Receive(newBuffer);        
                return new Data(code, length, newBuffer);
            }
            return new Data(0, 0, new byte[0]);
        }

        public void Disconnect()
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }


}
