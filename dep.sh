#!/bin/bash

py -m pip install altgraph
py -m pip  install backcall
py -m pip  install bleach
py -m pip  install build
py -m pip  install certifi
py -m pip  install chardet
py -m pip  install charset-normalizer
py -m pip  install colorama
py -m pip  install contourpy
py -m pip  install cycler
py -m pip  install decorator
py -m pip  install docutils
py -m pip  install fonttools
py -m pip  install future
py -m pip  install idna
py -m pip  install importlib-metadata
py -m pip  install importlib-resources
py -m pip  install imutils
py -m pip  install ipython
py -m pip  install ipython-genutils
py -m pip  install jaraco.classes
py -m pip  install jedi
py -m pip  install keyring
py -m pip  install kiwisolver
py -m pip  install markdown-it-py
py -m pip  install matplotlib
py -m pip  install mdurl
py -m pip  install more-itertools
py -m pip  install mpmath
py -m pip  install numpy
py -m pip  install opencv-python
py -m pip  install packaging
py -m pip  install parso
py -m pip  install pefile
py -m pip  install pickleshare
py -m pip  install Pillow
py -m pip  install pkginfo
py -m pip  install prompt-toolkit
py -m pip  install Pygments
py -m pip  install pyinstaller
py -m pip  install pyinstaller-hooks-contrib
py -m pip  install pyparsing
py -m pip  install pyproject_hooks
py -m pip  install python-dateutil
py -m pip  install pywin32-ctypes
py -m pip  install readme-renderer
py -m pip  install requests
py -m pip  install requests-toolbelt
py -m pip  install rfc3986
py -m pip  install rich
py -m pip  install scapy
py -m pip  install setuptools
py -m pip  install six
py -m pip  install sympy
py -m pip  install tomli
py -m pip  install tqdm
py -m pip  install traitlets
py -m pip  install twine
py -m pip  install typing_extensions
py -m pip  install urllib3
py -m pip  install wcwidth
py -m pip  install webencodings
py -m pip  install zipp
