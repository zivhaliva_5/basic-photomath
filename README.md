<a id="readme-top"></a>

<!-- PROJECT LOGO -->
<br>
<div align="center">
  <a href="https://gitlab.com/aurariolu2005/photomap-copy">
    <img src="resources\logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">PhotoMath-Copy</h3>

  <p align="center">
    <a href="https://gitlab.com/aurariolu2005/photomap-copy"><strong>Explore the docs »</strong></a>
    <br>
    <br>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#installation">Installation</a>
      <ul>
        <li><a href="#dependencies">Dependencies</a></li>
        <li><a href="#finally">Finally</a></li>
      </ul>
    </li>
    <li><a href="#activation">Activation</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

This project is inspired by the application Photomath, hence the name 'Photomath-Copy'.<br>
In this project we use OpenCV to separate an image that contains an equation into smaller images,<br>
 so that way each small image will contain one symbol/number from the equation.<br>
Afterwards we use an AI model to identify each symbol, and finally we use Sympy to solve the equation.<br>
the symbols/numbers we can identify are as follows: x y 1 2 3 4 5 6 7 8 9 0 = / - +.<br>
x is our multiplication symbol, and y is our variable.<br>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Built With

* [![Tensorflow][Tensorflow.py]][Tensorflow-url]
* [![opencv][opencv.py]][opencv-url]
* [![python][python.py]][python-url]
* [![numpy][numpy.py]][numpy-url]
* [![sympy][sympy.py]][sympy-url]
* [![c#][c#.cs]][c#-url]
* [![Net][Net.cs]][Net-url]
* [![WinForms][WinForms.cs]][WinForms-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# Installation

## Dependencies

* python version 3.8 you can install in the link below
  https://www.python.org/downloads/release/python-380/ <br>

* cuDNN version 8.1 <br>
  CUDA version 11.2 <br>
  you can install by following the guide in the following video
  https://www.youtube.com/watch?v=hHWkvEcDBO0&t=304s
 
* opencv : pythonic distribution
  ```sh
  pip install opencv-python
  ```
* inutils 
  ```sh
  pip install imutils
  ```
* numpy 
  ```sh
  pip install numpy
  ```  
  * mathplotlib 
  ```sh
  pip install mathplotlib
  ``` 
* tensorflow 
  ```sh
  pip install tensorflow-gpu==2.10
  ```
* sympy 
  ```sh
  pip install sympy
  ```
* tqdm 
  ```sh
  pip install tqdm
  ```
* pickle 
  ```sh
  pip install pickle
  ```
* `.NET` Framework: Newtonsoft.Json.13.0.2

## Finally 
* Clone the repo

* ```sh
  git clone https://gitlab.com/aurariolu2005/photomap-copy
  ```

<br>

# Activation
* In the folder `Server` under `ServerLogic` activate `main.py` or just in the folder of the project open cmd and enter the command

* After activating the server run `Client.exe` from `Client\bin\Release` folder

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

Project Link: [https://gitlab.com/aurariolu2005/photomap-copy](https://gitlab.com/aurariolu2005/photomap-copy)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[TensorFlow.py]:https://img.shields.io/badge/-Tensorflow-important
[TensorFlow-url]:https://www.tensorflow.org/?hl=he

[Python.py]:https://img.shields.io/badge/-Python-brightgreen
[Python-url]:https://www.python.org/

[Numpy.py]:https://img.shields.io/badge/-Numpy-orange
[Numpy-url]:https://numpy.org/

[Sympy.py]:https://img.shields.io/badge/-Sympy-blue
[Sympy-url]:https://www.sympy.org/en/index.html

[opencv.py]:https://img.shields.io/badge/-OpenCV-blueviolet
[opencv-url]:https://opencv.org/

[C#.cs]:https://img.shields.io/badge/-C%23-informational
[C#-url]:https://dotnet.microsoft.com/en-us/languages/csharp

[Net.cs]:https://img.shields.io/badge/-.Net-lightgrey
[Net-url]:https://dotnet.microsoft.com/en-us/

[WinForms.cs]:https://img.shields.io/badge/-WinForms-9cf
[WinForms-url]:https://learn.microsoft.com/en-us/dotnet/desktop/winforms/overview/?view=netdesktop-7.0
